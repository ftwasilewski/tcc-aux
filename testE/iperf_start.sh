#!/bin/bash

TIME='120'

ssh -f vm01a "date > iperf_vm01a.out; iperf -s -y c -i 1 >> iperf_vm01a.out &"
ssh -f vm02b "date > iperf_vm02b.out; iperf -c vm01a -y c -i 1 -t $TIME >> iperf_vm02b.out &"

ssh -f vm03a "date > iperf_vm03a.out; iperf -s -y c -i 1 >> iperf_vm03a.out &"
ssh -f vm04b "date > iperf_vm04b.out; iperf -c vm03a -y c -i 1 -t $TIME >> iperf_vm04b.out &"

ssh -f vm05a "date > iperf_vm05a.out; iperf -s -y c -i 1 >> iperf_vm05a.out &"
ssh -f vm06b "date > iperf_vm06b.out; iperf -c vm05a -y c -i 1 -t $TIME >> iperf_vm06b.out &"
