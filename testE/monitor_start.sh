#!/bin/bash

VMS=('vm01a' 'vm02b' 'vm03a' 'vm04b' 'vm05a' 'vm06b')
HOSTS=('nas1' 'nas2')

# Monitoramento nas vms
for i in "${VMS[@]}"
do
	ssh -f $i "/root/mon_scripts/capBW.sh > /root/capBW$i.out &"
	ssh -f $i "/root/mon_scripts/capBacklog.sh > /root/capBacklog$i.out &"
	ssh -f $i "/root/mon_scripts/capErr.sh > /root/capErr$i.out &"
	ssh -f $i "/root/mon_scripts/capDrop.sh > /root/capDrop$i.out &"
done

# Monitoramento nos Hosts
for j in "${HOSTS[@]}"
do
	ssh -f $j "xentop -d 1 -n -b > /root/fernando/xentop$j.out &"
done
