#!/usr/bin/gnuplot
reset
set terminal pngcairo dashed size 1280,480
set xlabel "Tempo (segundos)"
set ylabel "Largura de banda (Mbits/s)"
set yrange [0:1100]
set xrange [0:120]
set datafile separator ","
set grid
#unset key
set key horizontal right
set ytics "100"
set xtics "30"

set label "Início do 1º conjunto\n   de requisições" at 23,870
set label "Início do 2º conjunto\n    de requisições \n       e fim do 1º" at 53,870
set label "Fim do 2º conjunto\n   de requisições" at 83,870

set arrow from 30,0 to 30,740 lw 3 size screen 0.015,25,35
set arrow from 60,0 to 60,740 lw 3 size screen 0.015,25,35
set arrow from 90,0 to 90,740 lw 3 size screen 0.015,25,35

set output "results/chart_ser.png"

plot "results/iperf_vm01a.out" using ($0):($9/1000000) every ::1 title "Fluxo 1 - TCP" with lines lw 2, \
     "results/iperf_vm03a.out" using ($0):($9/1000000) every ::1 title "Fluxo 2 - TCP" with lines lw 2, \
     "results/iperf_vm05a.out" using ($0):($9/1000000) every ::1 title "Fluxo 3 - TCP" with lines lw 2

