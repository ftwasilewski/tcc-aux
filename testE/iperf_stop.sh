#!/bin/bash

VMS=('vm01a' 'vm02b' 'vm03a' 'vm04b' 'vm05a' 'vm06b')

for vm in "${VMS[@]}"
do
	ssh $vm "pkill iperf"
done
