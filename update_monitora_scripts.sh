#!/bin/bash

VMS=('vm01a' 'vm02a' 'vm03a' 'vm04a' 'vm05a' 'vm06a' 'vm01b' 'vm02b' 'vm03b' 'vm04b' 'vm05b' 'vm06b')

for i in "${VMS[@]}"
do
	#scp monitor_scripts/mon_scripts/capBW.sh $i:/root/mon_scripts/
	#scp monitor_scripts/mon_scripts/capBacklog.sh $i:/root/mon_scripts/
	scp monitor_scripts/mon_scripts/* $i:/root/mon_scripts/
done

