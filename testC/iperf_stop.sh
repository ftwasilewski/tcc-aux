#!/bin/bash

VMS=('vm01a' 'vm02a' 'vm03a' 'vm04a' 'vm05a' 'vm06a' 'vm03b' 'vm04b')

for vm in "${VMS[@]}"
do
	ssh $vm "pkill iperf"
done
