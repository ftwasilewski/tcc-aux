#!/bin/bash

TIME='120'

ssh -f vm01a "date > iperf_vm01a.out; iperf -s -y c -i 1 >> iperf_vm01a.out &"
ssh -f vm02a "date > iperf_vm02a.out; iperf -c vm01a -y c -i 1 -t $TIME >> iperf_vm02a.out &"

ssh -f vm03a "date > iperf_vm03a.out; iperf -s -y c -i 1 >> iperf_vm03a.out &"
ssh -f vm03b "date > iperf_vm03b.out; iperf -c vm03a -y c -i 1 -t $TIME >> iperf_vm03b.out &"

#ssh -f vm04a "date > iperf_vm04a.out; iperf -s -u -i 1 -y c >> iperf_vm04a.out &"
#ssh -f vm04b "date > iperf_vm04b.out; iperf -c vm04a -u -b1G -l1500 -i 1 -t $TIME -y c >> iperf_vm04b.out &"

#ssh -f vm05a "date > iperf_vm05a.out; iperf -s -u -i 1 -y c >> iperf_vm05a.out &"
#ssh -f vm06a "date > iperf_vm06a.out; iperf -c vm05a -u -b1G -l1500 -i 1 -t $TIME -y c >> iperf_vm06a.out &"

