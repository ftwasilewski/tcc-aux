#!/bin/bash

VMS=('vm01a' 'vm02a' 'vm03a' 'vm04a' 'vm05a' 'vm06a' 'vm03b' 'vm04b')
HOSTS=('nas1' 'nas2')

# Monitoramento nas vms
for i in "${VMS[@]}"
do
	ssh -f $i "/root/mon_scripts/capBW.sh > /root/capBW$i.out &"
	ssh -f $i "/root/mon_scripts/capBacklog.sh > /root/capBacklog$i.out &"
done

# Monitoramento nos Hosts
for j in "${HOSTS[@]}"
do
	ssh -f $j "xentop -d 1 -n -b > /root/fernando/xentop$j.out &"
done
