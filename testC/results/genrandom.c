/*
 * Programa para criar arquivos com conteudo aleatorio. 
 * 
 * Limitado a arquivos < 2 GB (POSIX). Para arquivos >= 2 GB, compilar
 * com
 *
 *   $ cc -D_FILE_OFFSET_BITS=64 -Wall -g -o genrandom genrandom.c 
 *
 */ 

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <time.h>

int
main(int argc, char **argv) {
	unsigned long long bytes;
	unsigned int k;
	char *endp;
	FILE *fp;

	if (argc != 3) {
		printf("usage: genrandom k file\n");
		exit(1);
	}
	k = strtoul(argv[1], &endp, 10);
	if (*endp != 0) {
		printf("usage: genrandom k file\n");
		exit(1);
	}
	bytes = ((unsigned long long) k) << 10;
	printf("k=%u, bytes=%llu\n", k, bytes);

	fp = fopen(argv[2], "w");
	if (fp == NULL) {
		printf("failed to open %s\n", argv[2]);
		exit(1);
	}

	srand(0x12345678);
	while (bytes > 0) {
		unsigned short int x = (rand() & 0xFFFF);
		unsigned char c = x & 0xFF;
		if (putc(c, fp) == EOF) {
			printf("error writing to file\n");
			exit(1);
		}
		c = x >> 8;
		if (putc(c, fp) == EOF) {
			printf("error writing to file\n");
			exit(1);
		}
		bytes -= 2;
	}
	fclose(fp);

	return (0);
}

