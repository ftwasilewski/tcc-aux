#!/usr/bin/gnuplot
reset
set terminal pngcairo dashed size 1280,480
set xlabel "Tempo (segundos)"
set ylabel "Largura de banda (Mbits/s)"
set yrange [0:7000]
set xrange [0:120]
set datafile separator ","
set grid
#unset key
set key horizontal right
set ytics "1000"
set xtics "30"

set label "Início do 1º conjunto\n   de requisições" at 23,5000
set label "Início do 2º conjunto\n    de requisições \n       e fim do 1º" at 53,5000
set label "Fim do 2º conjunto\n   de requisições" at 83,5000

set arrow from 30,0 to 30,4000 lw 3 size screen 0.015,25,35
set arrow from 60,0 to 60,4000 lw 3 size screen 0.015,25,35
set arrow from 90,0 to 90,4000 lw 3 size screen 0.015,25,35


set output "results/chart_cli.png"

plot "results/iperf_vm02a.out" using ($0):($9/1000000) every ::1 title "Fluxo 1 - TCP" with lines lw 2, \
     "results/iperf_vm03b.out" using ($0):($9/1000000) every ::1 title "Fluxo 2 - TCP" with lines lw 2, \
     "results/iperf_vm04b.out" using ($0):($9/1000000) every ::1 title "Fluxo 3 - UDP" with lines lw 2, \
     "results/iperf_vm06a.out" using ($0):($9/1000000) every ::1 title "Fluxo 4 - UDP" with lines lw 2

