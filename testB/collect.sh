#!/bin/bash

TEST='testB'
VMS=('vm01a' 'vm02b' 'vm03a' 'vm04b' 'vm05a' 'vm06b')
HOSTS=('nas1' 'nas2')

for i in "${VMS[@]}"
do
	scp $i:/root/* /root/fernando/$TEST/results
done

for j in "${HOSTS[@]}"
do
        scp $j:/root/fernando/* /root/fernando/$TEST/results
done
