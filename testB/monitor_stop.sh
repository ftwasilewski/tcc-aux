#!/bin/bash

VMS=('vm01a' 'vm02b' 'vm03a' 'vm04b' 'vm05a' 'vm06b')
HOSTS=('nas1' 'nas2')

for vm in "${VMS[@]}"
do
	ssh $vm "pkill capBW.sh; pkill capBacklog.sh"
done

for j in "${HOSTS[@]}"
do
        ssh -f $j "pkill xentop"
done

