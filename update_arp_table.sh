#!/bin/bash

HOSTS=('nas1' 'nas2')

for i in "${HOSTS[@]}"
do
	ssh $i "arp -s vm01a 00:16:3e:68:cf:35"
	ssh $i "arp -s vm02a 00:16:3e:cd:41:bf"
	ssh $i "arp -s vm03a 00:16:3e:55:18:59"
	ssh $i "arp -s vm04a 00:16:3e:d3:9e:fb"
	ssh $i "arp -s vm05a 00:16:3e:b2:c2:79"
	ssh $i "arp -s vm06a 00:16:3e:61:9f:96"
	ssh $i "arp -s vm01b 00:16:3e:e4:bc:e1"
	ssh $i "arp -s vm02b 00:16:3e:19:98:77"
	ssh $i "arp -s vm03b 00:16:3e:9d:93:91"
	ssh $i "arp -s vm04b 00:16:3e:d3:9e:ab"
	ssh $i "arp -s vm05b 00:16:3e:e1:a0:7f"
	ssh $i "arp -s vm06b 00:16:3e:c4:c5:8b"
done

