#!/bin/bash

echo "Removing results"
rm results/*

echo "Clear config"
./clear_config.sh

echo "Starting monitor"
./monitor_start.sh

echo "Starting iperf"
./iperf_start.sh

echo "Sleepgin 30"
sleep 30

echo "Config 1"
./config1.sh

echo "Sleeping 30"
sleep 30

echo "Config 2"
./config2.sh

echo "Sleeping 30"
sleep 30

echo "Clear config"
./clear_config.sh

echo "Sleeping 30"
sleep 30

echo "Stopping iperf"
./iperf_stop.sh

echo "Stopping monitor"
./monitor_stop.sh

echo "Collecting date from VMs"
./collect.sh

echo "Making charts"
./make_charts.sh
