#!/bin/bash

VMS=('vm01a' 'vm02a' 'vm03a' 'vm04a' 'vm05a' 'vm06a')
HOSTS=('nas1')

for vm in "${VMS[@]}"
do
	ssh $vm "pkill capBW.sh; pkill capBacklog.sh; pkill capErr.sh; pkill capDrop.sh"
done

for j in "${HOSTS[@]}"
do
        ssh -f $j "pkill xentop"
done

