#!/bin/bash

# Remove tcc /fernando/tcc directory and download a new version

# Update this machine
rm -r /root/fernando/tcc
git clone https://ftwasilewski@bitbucket.org/ftwasilewski/tcc.git

# Unpdate remote machines 
ssh nas1 "cd fernando; rm -r tcc; git clone https://ftwasilewski@bitbucket.org/ftwasilewski/tcc.git;"
ssh nas2 "cd fernando; rm -r tcc; git clone https://ftwasilewski@bitbucket.org/ftwasilewski/tcc.git;"

