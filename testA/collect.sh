#!/bin/bash

TEST='testA'
VMS=('vm01a' 'vm02a' 'vm03a' 'vm04a' 'vm05a' 'vm06a')
HOSTS=('nas1')

for i in "${VMS[@]}"
do
	scp $i:/root/* /root/fernando/$TEST/results
done

for j in "${HOSTS[@]}"
do
        scp $j:/root/fernando/* /root/fernando/$TEST/results
done

