#!/bin/bash

VMS=('vm01a' 'vm02a' 'vm03a' 'vm04a' 'vm05a' 'vm06a')

for vm in "${VMS[@]}"
do
	ssh $vm "pkill iperf"
done
